import React, { Fragment } from 'react';
import { Nav } from '../components/Navbar';
import { Intro, About } from '../components/Intro';
import { Skills, Projects } from '../components/Work';
import { Footer, Contact } from '../components/Footer';
import { about, contact, experience, intro, navigation, projects, SEO, work, skillset, frameworks, databases, tools } from '../config/config';
import { Header } from '../components/Header';
import { Experiences } from '../components/Experience';
import { SkillSet } from '../components/Languages';
import { Frameworks } from '../components/Frameworks';
import ScrollToTopButton from '../components/ScrollToTop';
import { Tools } from '../components/Tools';
import { DBs } from '../components/Databases';
import styles from './Index.module.css'
import book from "../public/oldbook.webp"
import Image from 'next/image';
import BottomNav from '../components/BottomNav';

export default function Home() {
  return (
    <Fragment>
      <Header seo={SEO} />
      <div style={{backgroundColor:"#f6f6f2"}}>
      <div style={{cursor:"none"}} className={styles.bookpage}>
      {/* <Image
        src={book} // Path relative to the public directory
        alt="Background Image"
        layout="fill"
        objectFit="contain"
        quality={100}
      /> */}
      <Nav
        title={navigation.name}
        links={navigation.links}
      />

      
      <Intro
        title={intro.title}
        description={intro.description}
        image={intro.image}
        buttons={intro.buttons}
      />
      <About
        title={about.title}
        description={about.description}
      />
      <Skills
        title={work.title}
        cards={work.cards}
      />

      <SkillSet 
        skills={skillset.skills}
      />

      <Frameworks 
        skills={frameworks.skills}
      />

      <DBs
        skills={databases.skills}
      />

      <Tools
        skills={tools.skills}
      />

      <Experiences
        title={experience.title}
        cards={experience.cards}
      />
      <Projects
        title={projects.title}
        cards={projects.cards}
      />
      <Contact
        title={contact.title}
        description={contact.description}
        buttons={contact.buttons}
      />

      <Footer />
      </div>
      <BottomNav />
      </div>
    </Fragment>
  );
}
