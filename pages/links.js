import React, { Fragment } from 'react';
import getConfig from 'next/config'
import Link from 'next/link';
import { Footer } from '../components/Footer';
import { links, SEO, } from '../config/config';
import { Header } from '../components/Header';
import { Router, useRouter } from 'next/router';
import Image from 'next/image';
import { faGithub } from '@fortawesome/free-brands-svg-icons';
import github from "../config/github.png"
import gitlab from "../config/gitLab.png"
import linkedin from "../config/linkedin.png"
import CustomCursor from '../components/CustomCursor';

const { publicRuntimeConfig } = getConfig()


const getLinkImage = (title) => {
  if(title == "My Github"){
    return (
      <Image 
            src={github}
            alt='xx'
            width={50}
            height={50}
          />
    )
  }else if(title == "My Gitlab"){
    return(
      <Image 
            src={gitlab}
            alt='xx'
            width={50}
            height={50}
          />
    )
  }
  return (
    <Image 
            src={linkedin}
            alt='xx'
            width={50}
            height={50}
          />
  )
}

export default function Home() {
  const router = useRouter()

  return (
    <Fragment>
      <Header seo={SEO} />
      <h3 className="mt-3"
      style={{cursor:"pointer", marginLeft: 20}}
      onClick={()=>{
        router.push('/')
      }}
      >&larr;</h3>
      <div className="d-flex flex-column justify-content-between bg-secondary min-vh-100" style={{cursor:"none"}}>
        <div className="py-5 px-5 container text-center">
          <img className="img-fluid my-3 card-image" width="150" height="150" src={links.image} alt="profile of hashirshoaeb" />
          <h3 className="mt-3">{links.title}</h3>
          <p>{links.description}</p>
          {links.cards.map((value, index) => (
            <Button key={index} title={value.title} link={value.link} />
          ))}
        </div>
        <Footer />
        <CustomCursor />
      </div>
    </Fragment>
  );
}


function Button({ title, link, image }) {
  return (
    <div className="row justify-content-center">
      <div className="card card-work mx-sm-4 my-2 py-2" style={{ width: "10rem" }}>
        <Link href={link}>
          <a target="_blank" rel="noreferrer">
            {/* <h4 className="text-primary py-3 px-3">{title}</h4> */}
            {getLinkImage(title)}
          </a>
        </Link>
      </div>
    </div>
  );
}