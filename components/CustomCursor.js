
import { useEffect, useState } from 'react';
import styles from './CustomCursor.module.css';
import Image from 'next/image';
import stick from '../public/handwithstick.png'

const CustomCursor = () => {
  const [position, setPosition] = useState({ x: 0, y: 0 });

  const updateCursorPosition = (e) => {
    setPosition({ x: e.clientX, y: e.clientY });
  };

  const handleClick = () => {
    console.log('Cursor clicked!');
  };

  useEffect(() => {
    document.addEventListener('mousemove', updateCursorPosition);

    return () => {
      document.removeEventListener('mousemove', updateCursorPosition);
    };
  }, []);

  return (
    <div className={styles.customCursor} style={{ left: `${position.x}px`, top: `${position.y}px` }}>
      <button className={styles.clickableArea} onClick={handleClick}></button>
   <Image 
      src={stick}
      width={150}
      height={150}
      />
      🖱️
    </div>
  );
};

export default CustomCursor;
