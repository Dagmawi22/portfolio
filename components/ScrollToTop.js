import { useState, useEffect } from 'react';
import styles from './Scroll.module.css'; // Import your styles

const ScrollToTopButton = () => {
  const [isVisible, setIsVisible] = useState(false);

  const handleScroll = () => {
    const scrollPosition = window.scrollY;
    console.log("pos", scrollPosition)
    setIsVisible(scrollPosition > 300);
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <button
      className={`${styles.scrollToTopButton} ${isVisible ? styles.visible : ''}`}
      onClick={scrollToTop}
    >
      <span style={{fontSize: 30}}>&uarr;</span>
    </button>
  );
};

export default ScrollToTopButton;
