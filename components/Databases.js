
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import Image from "next/image";

export const Experiences = ({ title, cards }) => {
  return (
    <div id="experiences" className="bg-secondary py-5 px-5">
      <div className="container">
        <h1 className="text-primary fw-bold">{title}</h1>
        <div className="d-flex flex-row flex-wrap justify-content-center">
          {cards.map((value, index) => (
            <Card
              key={index}
              title={value.title}
              description={value.description}
              link={value.link}
              company={value.company}
              date={value.date}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export const DBs = ({ skills }) => {
  return (
    <div id="skillset" className="bg-primary py-5 px-5">
      <div className="container">
        <h1 className="text-light fw-bold">Databases</h1>
        <div className="d-flex flex-row flex-wrap justify-content-center">
          {skills.map((value, index) => (
            <Card
              key={index}
              title={value.title}
              image={value.image}
            //   description={value.description}
            //   icons={value.icons}
            />
          ))}
        </div>
        {/* <div className="text-center">
					<button type="button" className="btn btn-outline-light">See More</button>
				</div> */}
      </div>
    </div>
  );
};

export const Card = ({ title, image }) => {
  return (
    <div
      className="card py-3 px-3 mx-sm-4 my-4 card-work"
      style={{ width: "20rem" }}
    >
      <Image
              src={image}
              alt={title}
              width={"100%"}
              height={100}
            //   layout="fill"
            />

     
      {/* <div className="text-center">
        
              <a target="_blank" rel="noreferrer">
                <span>{title}</span>
              </a>
      </div> */}
      
    </div>
  );
};
