import profile from "./photo-removebg-preview.png";
import {
  faAppStore,
  faGithub,
  faGooglePlay,
  faCreativeCommonsSamplingPlus,
} from "@fortawesome/free-brands-svg-icons";
import {} from "@fortawesome/free-solid-svg-icons";

//skills

import figma from "../components/skills/figma.png"
import shopify from "../components/skills/shopify.png"
import js from "../components/skills/js.png"
import java from "../components/skills/java.png"
import laravel from "../components/skills/laravel.png"
import ts from "../components/skills/ts.png"
import kotlin from "../components/skills/kotlin.png"
import mui from "../components/skills/mui.png"
import nextjs from "../components/skills/nextjs.png"
import react from "../components/skills/react.png"
import reactnative from "../components/skills/reactnative.png"
import express from "../components/skills/expressjs.png"
import mongo from "../components/skills/mongo.png"
import pg from "../components/skills/pg.png"
import php from "../components/skills/php.png"
import symfony from "../components/skills/symfony.png"
import mysql from "../components/skills/mysql.png"
import firebase from "../components/skills/firebase.png"
import sqlite from "../components/skills/sqlite.jpeg"
import redis from "../components/skills/redis.png"
import babel from "../components/skills/babel.png"
import webpack from "../components/skills/webpack.png"
import git from "../components/skills/git.png"
import python from "../components/skills/python.png"
import vue from "../components/skills/vuee.png"
import jquery from "../components/skills/jquery.png"
import bootstrap from "../components/skills/bootstrap.png"
import docker from "../components/skills/docker.png"

import linkedin from "./linkedin.png"
import github from "./github.png"
import gitlab from "./gitLab.png"

export const navigation = {
  name: "Dagmawi",
  links: [
    {
      title: "About",
      link: "#about",
    },
    {
      title: "Skills",
      link: "#skillset",
    },
    {
      title: "Experience",
      link: "#experiences",
    },
    {
      title: "Projects",
      link: "#projects",
    },
    {
      title: "Contact",
      link: "#contact",
    },
    {
      title: "Links",
      link: "/links",
    },
  ],
};
export const intro = {
  title: "Hello World",
  heading: "It's Dagmawi Teka",
  description: "A Software Engineer building MOBILE and WEB apps.",
  image: profile.src,
  buttons: [
    {
      title: "Contact Me",
      link: "#contact",
      isPrimary: true,
    },
    {
      title: "Resume",
      link: "https://drive.google.com/file/d/1sVXH_S1xuK9AEDCZf0-G2beu9o4fzVgk/view?usp=drive_link",
      isPrimary: false,
    },
  ],
};

export const about = {
  title: "Who is he",
  description: [
    "Software Engineer specializing in web and mobile app development.",
    "Well-versed in numerous programming languages, frameworks and standards.",
    "Loves working in the descipline starting from software architecture and to final product for end users.",
  ],
};

export const work = {
  title: "What he does",
  cards: [
    {
      title: "Mobile App Development",
      description:
        "Creates pixel perfect iOS and Andriod apps using React-Native and Java/Kotlin.",
      icons: null,
    },
    {
      title: "Web Development",
      description: "Builds responsive and user-centered web apps.",
      icons: null,
    },
    {
      title: "UX Design",
      description:
        "Innovates engaging user experience for software products by giving ultimate care to users' perspective.",
      icons: null,
    },
    {
      title: "Sofware Architectural design",
      description: "Drafts high and low level designs for software.",
      icons: null,
    },
  ],
};

export const experience = {
  title: "Professional Experience",
  cards: [
    {
      title: "Senior Software Engineer",
      company: "Borsa Technologies",
      date: "Feb 2023 - present",
      description:
        [
          "Working as a Front-end developer in a US based Ethiopian start-up",
          "Getting involved in the whole making of an app focusing on logistics of items world-wide",
        ],
      icons: null,
    },
    {
      title: "Software Developer",
      company: "Africa Jobs Network (Ethiojobs.net)",
      date: "Sep 2022 - present",
      description:
        [
          "Working as a Full-Stack software developer",
          "Significantly contributing to a multi-national ATS + Job board system called Zebra jobs including ethiojobs, kipawa with their respective ATS systems to be used by companies side",
        ],
      icons: null,
    },
    {
      title: "Freelance Software Developer",
      company: "Self-employed",
      date: "Nov 2022 - present",
      description:
      [
        "Working as a self-employed Software Developer & UI/UX Designer",
      ],
      icons: null,
    },
    // {
    //   title: "Sofware Architectural design",
    //   description: "I create high and low level designs for software.",
    //   icons: null,
    // },
  ],
};

export const projects = {
  title: "Projects",
  cards: [
    {
      title: "Yetalle",
      description:
        "A bussiness review platform for users to access what is in their surrounding and for businesses to showcase their work.",
      icons: [
        {
          icon: faCreativeCommonsSamplingPlus,
          link: "http://yetalle.42web.io",
        },
      ],
    },
    {
      title: "Mamelkecha",
      description:
        "Resume builder application that generates resumes with awesome templates.",
      icons: [
        {
          icon: faAppStore,
          link: "https://mamelkecha.vercel.app",
        },
      ],
    },
    {
      title: "Zebra",
      description:
        "A hr solution provider for organizations including: job posting, filtering candidates, company branding, cv search.",
      icons: [
        {
          icon: faGithub,
          link: "https://app1.zebra-app.dev",
        },
      ],
    },

    {
      title: "Mall Navigation System (pending)",
      description:
        "A system showing a map view of every mall in Ethiopia and what shops are in it using a 2D/3D view.",
      icons: [
        {
          icon: faAppStore,
          link: "",
        },
      ],
    },

    {
      title: "Lwe (pending)",
      description:
        "A mobile app for swapping items we no longer use with others.",
      icons: [
        {
          icon: faGithub,
          link: "",
        },
      ],
    },
  ],
};

export const contact = {
  title: "Get in touch",
  description:
    "Please do not hesitate to schedule a meeting. Alternatively, feel free to reach out directly by email. Buying me a coffee would be much appreciated!",
  buttons: [
    {
      title: "Email Me",
      link: "mailto:dagmawi.teka22@gmail.com",
      isPrimary: true,
    },
    {
      title: "Call",
      link: "tel:+251918888225",
      isPrimary: false,
    },
  ],
};

// SEARCH ENGINE
export const SEO = {
  // 50 - 60 char
  title: "Dagmawi Teka | Software Developer | React | Laravel",
  description:
    "I create mobile apps and websites. I graduated from University of Gondar in 2021 with a degree in Computer Science.",
  image: profile.src,
};

export const links = {
  image: profile.src,
  title: "@dagmawi_teka",
  description: "Software Engineer",
  cards: [
    {
      title: "My Gitlab",
      link: "https://gitlab.com/Dagmawi22",
      image: gitlab
    },
    {
      title: "My Github",
      link: "https://github.com/Dagmawi-22",
      image: github
    },
    {
      title: "My LinkedIn",
      link: "https://www.linkedin.com/in/dagmawi-teka",
      image: linkedin
    },
  ],
};

export const skillset = {
  skills: [
    // {
    //   title: 'Express.js',
    //   image: express
    // },
    // {
    //   title: 'Figma',
    //   image: figma
    // },
    {
      title: 'Java',
      image: java
    },
    {
      title: 'JavaScript',
      image: js
    },
    {
      title: 'Kotlin',
      image: kotlin
    },
    {
      title: 'php',
      image: php
    },
    {
      title: 'Python',
      image: python
    },
    // {
    //   title: 'Laravel',
    //   image: laravel
    // },
    // {
    //   title: 'Mongo DB',
    //   image: mongo
    // },
    // {
    //   title: 'MUI',
    //   image: mui
    // },
    // {
    //   title: 'Next.js',
    //   image: nextjs
    // },
    // {
    //   title: 'PostgreSql',
    //   image: pg
    // },
    // {
    //   title: 'React',
    //   image: react
    // },
    //{
    //   title: 'React Native',
    //   image: reactnative
    // },
    // {
    //   title: 'Shopify',
    //   image: shopify
    // },
    {
      title: 'TypeScript',
      image: ts
    },
  ],
};

export const frameworks = {
  skills: [
    {
      title: 'Express.js',
      image: express
    },
    // {
    //   title: 'Figma',
    //   image: figma
    // },
    // {
    //   title: 'Java',
    //   image: java
    // },
    // {
    //   title: 'JavaScript',
    //   image: js
    // },
    // {
    //   title: 'Kotlin',
    //   image: kotlin
    // },
    // {
    //   title: 'php',
    //   image: php
    // },
    {
      title: 'Laravel',
      image: laravel
    },
    // {
    //   title: 'Mongo DB',
    //   image: mongo
    // },
    // {
    //   title: 'MUI',
    //   image: mui
    // },
    {
      title: 'Next.js',
      image: nextjs
    },
    // {
    //   title: 'PostgreSql',
    //   image: pg
    // },
    {
      title: 'React',
      image: react
    },
    {
      title: 'React Native',
      image: reactnative
    },
    {
      title: 'Symfony',
      image: symfony
    },
    {
      title: 'Vue',
      image: vue
    },
    // {
    //   title: 'Shopify',
    //   image: shopify
    // },
    // {
    //   title: 'TypeScript',
    //   image: ts
    // },
  ],
};

export const databases = {
  skills: [
    // {
    //   title: 'Express.js',
    //   image: express
    // },
    // {
    //   title: 'Figma',
    //   image: figma
    // },
    // {
    //   title: 'Java',
    //   image: java
    // },
    // {
    //   title: 'JavaScript',
    //   image: js
    // },
    // {
    //   title: 'Kotlin',
    //   image: kotlin
    // },
    // {
    //   title: 'php',
    //   image: php
    // },
    // {
    //   title: 'Laravel',
    //   image: laravel
    // },
    {
      title: 'Firebase',
      image: firebase
    },
    {
      title: 'Mongo DB',
      image: mongo
    },
    {
      title: 'MySql',
      image: mysql
    },
    // {
    //   title: 'MUI',
    //   image: mui
    // },
    // {
    //   title: 'Next.js',
    //   image: nextjs
    // },
    {
      title: 'PostgreSql',
      image: pg
    },
    {
      title: 'Reddis',
      image: redis
    },
    {
      title: 'SqLite',
      image: sqlite
    },
    // {
    //   title: 'React',
    //   image: react
    // },
    // {
    //   title: 'React Native',
    //   image: reactnative
    // },
    // {
    //   title: 'Symfony',
    //   image: symfony
    // },
    // {
    //   title: 'Shopify',
    //   image: shopify
    // },
    // {
    //   title: 'TypeScript',
    //   image: ts
    // },
  ],
};

export const tools = {
  skills: [
    // {
    //   title: 'Express.js',
    //   image: express
    // },
    // {
    //   title: 'Babel',
    //   image: babel
    // },
    {
      title: 'Bootstrap',
      image: bootstrap
    },
    {
      title: 'Docker',
      image: docker
    },
    {
      title: 'Figma',
      image: figma
    },
    {
      title: 'Git',
      image: git
    },
    {
      title: 'JQuery',
      image: jquery
    },
    // {
    //   title: 'Java',
    //   image: java
    // },
    // {
    //   title: 'JavaScript',
    //   image: js
    // },
    // {
    //   title: 'Kotlin',
    //   image: kotlin
    // },
    // {
    //   title: 'php',
    //   image: php
    // },
    // {
    //   title: 'Laravel',
    //   image: laravel
    // },
    // {
    //   title: 'Firebase',
    //   image: firebase
    // },
    // {
    //   title: 'Mongo DB',
    //   image: mongo
    // },
    // {
    //   title: 'MySql',
    //   image: mysql
    // },
    {
      title: 'MUI',
      image: mui
    },
    // {
    //   title: 'Next.js',
    //   image: nextjs
    // },
    // {
    //   title: 'PostgreSql',
    //   image: pg
    // },
    // {
    //   title: 'Reddis',
    //   image: redis
    // },
    // {
    //   title: 'SqLite',
    //   image: sqlite
    // },
    // {
    //   title: 'React',
    //   image: react
    // },
    // {
    //   title: 'React Native',
    //   image: reactnative
    // },
    // {
    //   title: 'Symfony',
    //   image: symfony
    // },
    {
      title: 'Shopify',
      image: shopify
    },
    {
      title: 'Webpack',
      image: webpack
    },
    // {
    //   title: 'TypeScript',
    //   image: ts
    // },
  ],
};

// https://www.npmjs.com/package/@dropzone-ui/react#dropzone-ui-react-components-api
